import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SpotifyService {

  uri = 'https://api.spotify.com/v1/';

  constructor( private http: HttpClient ) { }

  getQuery( query: string ) {
    const url = `https://api.spotify.com/v1/${ query }`;
    const headers = new HttpHeaders({
      Authorization: 'Bearer BQAPSPur7iS8w3sWWBNRYOeez4H3RVHg55852R4SUQss7vJY1u1xuX2UzwOk-1_b1mxW60jgn15rVOUbMok' // TOKEN
    });

    return this.http.get(url, {headers});
  }

  getNewReleases() {
    return this.getQuery('browse/new-releases?limit=20')
      .pipe( map( response => response['albums'].items ));
  }

  getArtistas( termino: string ) {

    return this.getQuery( `search?q=${termino}&type=artist&limit=15`)
      .pipe( map( response => response['artists'].items ));
    /*return this.http.get( `https://api.spotify.com/v1/search?q=${ termino }&type=artist&limit=15`, { headers } )
      .pipe( map( response => {
        return response['artists'].items;
      }));*/
  }

  getArtista( id: string ) {
    return this.getQuery(`artists/${id}`);
      //.pipe(map(response => response['artists'].items));
  }

  getTopTracks( id: string ) {
    return this.getQuery( `artists/${id}/top-tracks?country=us` )
      .pipe( map( response => response['tracks'] ));
  }
}
