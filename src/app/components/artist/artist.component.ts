import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {SpotifyService} from '../../services/spotify.service';

@Component({
  selector: 'app-artist',
  templateUrl: './artist.component.html',
  styles: []
})
export class ArtistComponent implements OnInit {

  artista: any = {};
  toptracks: any[] = [];

  loading: boolean;

  constructor( private activatedRoute: ActivatedRoute,
               private spotifyService: SpotifyService ) { }

  ngOnInit() {

    this.loading = true;

    this.activatedRoute.params.subscribe(
      response => {
            this.getArtista( response['id'] );
            this.getTopTracks( response['id'] );
            this.loading = false;
      });
  }

  getArtista( id: string ) {
    this.spotifyService.getArtista( id )
      .subscribe( response => {
        this.artista = response;
      });
  }

  getTopTracks( id: string ) {
    this.spotifyService.getTopTracks( id )
      .subscribe( response => {
        this.toptracks = response;
        console.log(this.toptracks);
      });
  }

}
